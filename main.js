const electron = require('electron')
const { app, BrowserWindow } = electron

process.env['ELECTRON_DISABLE_SECURITY_WARNINGS'] = true;

const jsonfile = require('jsonfile')
const file = './data.json'

let win

function createWindow() {

    // Create the browser window.
    win = new BrowserWindow({
        webPreferences: {
            nodeIntegration: false
        },
        width: 1920,
        height: 1080,
        options: {
            //fullscreen: true
        }
    })

    jsonfile.readFile(file, (err, obj) => {
        if (obj != null) {
            win.loadURL('http://demo.dotworkers.com/usuarios/iniciar-sesion' , {
                postData: [{
                    type: 'rawData',
                    bytes: Buffer.from('username=' + obj.username + '&password=' + obj.password)
                }],
                extraHeaders: 'Content-Type: application/x-www-form-urlencoded'
            })
        }
        else
            console.log(err);
    })

    win.setMenu(null);

    //win.setFullScreen(true);
    //win.maximize();

}

app.on('ready', createWindow)
